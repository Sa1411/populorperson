package com.example.populorperson

data class Person(
    val page: Int,
    val results: List<Result>,
    val total_pages: Int,
    val total_results: Int
)