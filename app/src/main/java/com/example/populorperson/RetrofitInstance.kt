package com.example.populorperson

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitInstance {

        val api : PersonInterface by lazy {
            Retrofit.Builder()
                .baseUrl("https://api.themoviedb.org/3/person/")
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(PersonInterface::class.java)

    }

}