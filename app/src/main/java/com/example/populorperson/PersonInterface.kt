package com.example.populorperson

import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface PersonInterface{

        @GET("popular?")
        fun getPopularMovies(@Query("api_key") api_key : String) : Call<Person>


}